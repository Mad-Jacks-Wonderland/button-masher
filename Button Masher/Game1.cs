﻿using System;
using Microsoft.Xna.Framework.Media;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace Button_Masher
{
    /// <summary>
    /// This is the main type for your game.
    /// </summary>
    public class Game1 : Game
    {
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;

        //loaded in-game assets
        Texture2D buttonTexture;
        SpriteFont gameFont;
        SoundEffect clickSFX;
        SoundEffect gameEndSFX;
        Song gameMusic;


        //Game state / input

        MouseState previousState;

        //score
        int score = 0;

        //start game boolean
        bool playing = false;

        float timeRemaining = 0f;
        float timeLimit = 5f;

        public Game1()
        {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";

            
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            // TODO: Add your initialization logic here

            base.Initialize();
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            // Create a new SpriteBatch, which can be used to draw textures.
            spriteBatch = new SpriteBatch(GraphicsDevice);

            // TODO: use this.Content to load your game content here

            //Load button graphics
            buttonTexture = Content.Load<Texture2D>("graphics/button");

            //Load Font
            gameFont = Content.Load<SpriteFont>("fonts/MainSpriteFont");

            //Load Audio
            clickSFX = Content.Load<SoundEffect>("audio/buttonclick");

            gameEndSFX = Content.Load<SoundEffect>("audio/gameOver");

            gameMusic = Content.Load<Song>("audio/music");


            //start background music

            MediaPlayer.Play(gameMusic);
            MediaPlayer.IsRepeating = true;

            //make mouse visible
            IsMouseVisible = true;

           

            
        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// game-specific content.
        /// </summary>
        protected override void UnloadContent()
        {
            // TODO: Unload any non ContentManager content here
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed || Keyboard.GetState().IsKeyDown(Keys.Escape))
                Exit();

            // TODO: Add your update logic here

            //get current mouse state
            MouseState currentState = Mouse.GetState();

            

            Vector2 screenCentre = new Vector2
                (Window.ClientBounds.Width / 2,
                Window.ClientBounds.Height / 2);

            //Determine button rectangle
            Rectangle buttonRect = new Rectangle(
                (int)screenCentre.X - (buttonTexture.Width / 2),
                (int)screenCentre.Y - (buttonTexture.Height / 2),
                buttonTexture.Width,
                buttonTexture.Height);

            //Check if we have clicked the mouse
            if (currentState.LeftButton == ButtonState.Pressed &&
                previousState.LeftButton != ButtonState.Pressed &&
                buttonRect.Contains(currentState.X, currentState.Y))
            {
                clickSFX.Play();

                if (playing == true)
                {
                    ++score;
                }
                else
                {
                    playing = true;

                    // set time remaining to the full time limit when we start
                    timeRemaining = timeLimit;


                    score = 0;
                }



            }

            if (playing == true)
            {
                //update the time remaining
                //subtract the time passed this frame from the time remaining
                timeRemaining -= (float)gameTime.ElapsedGameTime.TotalSeconds;

                if (timeRemaining <= 0)
                {
                    playing = false;
                    timeRemaining = 0;
                    gameEndSFX.Play();
                }
            }

            previousState = currentState;

            base.Update(gameTime);
        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.CornflowerBlue);

            // TODO: Add your drawing code here

            spriteBatch.Begin();
            //Draw stuff

            Vector2 screenCentre = new Vector2
                (Window.ClientBounds.Width / 2, 
                Window.ClientBounds.Height / 2);

            //Determine button rectangle
            Rectangle buttonRect = new Rectangle(
                (int)screenCentre.X - (buttonTexture.Width / 2),
                (int)screenCentre.Y - (buttonTexture.Height / 2),
                buttonTexture.Width,
                buttonTexture.Height);

            spriteBatch.Draw(buttonTexture, new Rectangle(
                (int)screenCentre.X - (buttonTexture.Width / 2), 
                (int)screenCentre.Y - (buttonTexture.Height / 2), 
                buttonTexture.Width, 
                buttonTexture.Height), 
                Color.Yellow);

            Vector2 titleSize = gameFont.MeasureString("Button Masher");

            spriteBatch.DrawString(gameFont, "Button Masher", screenCentre - new Vector2(0, 100) - titleSize / 2, Color.White);

            spriteBatch.DrawString(gameFont, "By Maciej Wolski", screenCentre - new Vector2(9, 80) - titleSize / 2, Color.White);

            
            string promptString = "Press the button to start";
            if (playing == true)
                promptString = "Mash the button untill the time runs out!";
            Vector2 promptSize = gameFont.MeasureString(promptString);

            spriteBatch.DrawString(gameFont, promptString, screenCentre - new Vector2(0, 60) - promptSize / 2, Color.White);


            spriteBatch.DrawString(gameFont, "Score:" , new Vector2(10, 10), Color.White);
            spriteBatch.DrawString(gameFont, score.ToString(), new Vector2(80, 10), Color.White);

            spriteBatch.DrawString(gameFont, "Timer:", new Vector2(Window.ClientBounds.Width - 150, 10), Color.White);
            spriteBatch.DrawString(gameFont, timeRemaining.ToString(), new Vector2(Window.ClientBounds.Width - 50, 10), Color.White);


            //Stop Drawing
            spriteBatch.End();

            base.Draw(gameTime);
        }
    }
}
